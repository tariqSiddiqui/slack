import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var config = {
    apiKey: "AIzaSyBOG-pczfndYUieF9mCKJAuw70Px3Qtapw",
    authDomain: "slackclone-dbf5e.firebaseapp.com",
    databaseURL: "https://slackclone-dbf5e.firebaseio.com",
    projectId: "slackclone-dbf5e",
    storageBucket: "slackclone-dbf5e.appspot.com",
    messagingSenderId: "526897184613"
};
firebase.initializeApp(config);

export default firebase;
